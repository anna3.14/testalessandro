package org.example;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommitTest {
    Commit commit;

    @BeforeEach
    void setUp(){
        commit=new Commit("345fg","add metodo somma");
    }

    @Test
    void verificaProprieta(){
        assertNotNull(commit);
        assertEquals("345fg",commit.getiD());
        assertEquals("add metodo somma",commit.getMessaggio());
    }


}