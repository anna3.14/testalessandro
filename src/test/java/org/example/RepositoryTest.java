package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {

    Repository repository;
    Commit commit1;
    Commit commit2;

    @BeforeEach
    void setUp(){
        repository=new Repository();
        commit1=new Commit("453ty","add metodo somma");
        commit2=new Commit("453ar","add metodo sottrazione");

    }

    @Test
    void testAggiuntaCommits(){
        repository.aggiungiCommit(commit1);
        assertEquals(1,repository.getCommits().size());
        repository.aggiungiCommit(commit2);
        assertEquals(2,repository.getCommits().size());

    }

    @Test
    void testultimoCommits(){
        repository.aggiungiCommit(commit1);
        repository.aggiungiCommit(commit2);
        assertEquals(commit2,repository.ultimoCommit());
    }


}