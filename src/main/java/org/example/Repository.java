package org.example;

import java.util.ArrayList;
import java.util.List;
public class Repository {
    protected String nome;
    protected List<Commit> commits;

    public List<Commit> getCommits(){
        return commits;
    }

    public Repository() {

        commits = new ArrayList<>();
    }

    public void aggiungiCommit(Commit commit){
        commits.add(commit);
    }

    public Commit ultimoCommit() {
        Commit commit;
        if (!this.commits.isEmpty()) {
            commit = this.commits.get(this.commits.size() - 1);
            return commit;
        }
        else return null;
    }

    public int numeroDiCommit(){
        int numero = this.commits.size();
        return numero;
    }
}

