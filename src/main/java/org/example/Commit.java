package org.example;


public class Commit {
    protected String iD;
    protected String messaggio;
    public Commit(String iD, String messaggio){
        this.iD = iD;
        this.messaggio = messaggio;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }
}

